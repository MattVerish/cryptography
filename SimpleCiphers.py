shift_constant = 96
#Encrypts text by applying affine transform in form ax+b (mod 26)
def AffineCipher(word, a, b):
	newWord = ''
	for char in word.lower():
		wordNum = ord(char) - 96
		transNum = (a*wordNum + b) % 26
		newChar = transNum + 96
		newWord += chr(newChar)
	return newWord

def Encrypt(fileName, a, b):
	encryptedFile = open('CipherText/CipherText2.txt', "w+")
	file = open(fileName, 'r')
	for line in file:
		for word in line.split():
			encryptedFile.write(AffineCipher(word, a, b) + " ")

	encryptedFile.close()
	file.close()
Encrypt('CipherText/CipherText1.txt', -2, -5)